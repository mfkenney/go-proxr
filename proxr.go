// Proxr implements the communication protocol used by NCD Relay Boards.
package proxr

import (
	"github.com/pkg/errors"
	"net"
)

type Command []byte

const START byte = 254
const RESPONSE byte = 85

func Test() Command {
	return Command{START, 33}
}

func SetBank(bank byte) Command {
	return Command{START, 49, bank}
}

func GetBank() Command {
	return Command{START, 34}
}

func BankOff(bank byte) Command {
	return Command{START, 129, bank}
}

func RelayOff(relay, bank byte) Command {
	return Command{START, 100 + relay, bank}
}

func RelayOn(relay, bank byte) Command {
	return Command{START, 108 + relay, bank}
}

func RelayStatus(relay, bank byte) Command {
	return Command{START, 116 + relay, bank}
}

func ReadBank(bank byte) Command {
	return Command{START, 124, bank}
}

func WriteBank(value, bank byte) Command {
	return Command{START, 140, value, bank}
}

func ReadAdcChannel(channel byte) Command {
	return Command{START, 158 + channel}
}

func ReadAdcAll(channel byte) Command {
	return Command{START, 167}
}

func Send(conn net.Conn, cmd Command, nresp int) ([]byte, error) {
	var reply []byte
	_, err := conn.Write(cmd)
	if err != nil {
		return nil, errors.Wrap(err, "command send failed")
	}
	if nresp > 0 {
		reply = make([]byte, nresp)
		n, err := conn.Read(reply)
		if err != nil {
			return nil, errors.Wrap(err, "cannot read response")
		}
		if n < nresp {
			return reply, errors.New("short read")
		}
	}

	return reply, nil
}
