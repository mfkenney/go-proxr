// Implement the PowerSwitch interface using ProXR
package proxr

import (
	"github.com/pkg/errors"
	"net"
)

type Switch struct {
	conn        net.Conn
	bank, relay byte
}

func NewSwitch(conn net.Conn, relay, bank byte) (*Switch, error) {
	if relay >= 8 {
		return nil, errors.Errorf("invalid relay number: %d", relay)
	}
	return &Switch{
		conn:  conn,
		relay: relay,
		bank:  bank}, nil
}

func (sw *Switch) On() error {
	_, err := Send(sw.conn, RelayOn(sw.relay, sw.bank), 1)
	return err
}

func (sw *Switch) Off() error {
	_, err := Send(sw.conn, RelayOff(sw.relay, sw.bank), 1)
	return err
}

func (sw *Switch) Test() bool {
	resp, err := Send(sw.conn, RelayStatus(sw.relay, sw.bank), 1)
	if err != nil {
		return false
	}
	return resp[0] == byte(1)
}
